import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Add } from '../models/add';
import { Observable } from 'rxjs';
import { ICreateEmployeeResponse } from '../interfaces/icreate-employee-response';
import { IemployeeResponse } from '../interfaces/iemployee-response';
import { List } from '../models/list';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  private SAVE_API: string = "http://localhost:8080/employee/save";
  private GET_Employees_API: string = "http://localhost:8080/employee/list";
  private GET_API: string = "http://localhost:8080/employee/get/";
  private PUT_API: string = "http://localhost:8080/employee/updateEmployee";
  private DELETE_API: string = "http://localhost:8080/employee/deleteEmployee/";

  constructor(private _httpClient: HttpClient) { }

  public save(employee: Add): Observable<string> {
    return this._httpClient.post<string>(this.SAVE_API, employee);
  }

  public getEmployees(): Observable<ICreateEmployeeResponse> {
    return this._httpClient.get<ICreateEmployeeResponse>(this.GET_Employees_API);
  }

  public getEmployeeById(id): Observable<List> {
    return this._httpClient.get<List>(this.GET_API + id);
  }

  public updateEmployees(updateEmployee: Add): Observable<ICreateEmployeeResponse> {
    return this._httpClient.put<ICreateEmployeeResponse>(this.PUT_API, updateEmployee);
  }

  public deleteEmployee(id) {
    return this._httpClient.delete(this.DELETE_API + id);
  }
}
