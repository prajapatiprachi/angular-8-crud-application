import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  public authentication(userName, password) {
    if (userName && password) {
      sessionStorage.setItem('authenticatedUser', password);
      return true
    }
    return false
  }

  public isUserLoggedIn() {
    let user = sessionStorage.getItem('authenticatedUser')
    return !(user === null)
  }
  public logout() {
    return sessionStorage.removeItem('authenticatedUser');
  }
}
