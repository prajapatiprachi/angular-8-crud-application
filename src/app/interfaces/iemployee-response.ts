import {List} from "../models/list";

export interface IemployeeResponse {
    status: boolean
    data: List;
}

