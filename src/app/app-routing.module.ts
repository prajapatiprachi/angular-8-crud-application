import { NgModule } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute } from '@angular/router';
import { AddComponent } from './components/add/add/add.component';
import { UpdateComponent } from './components/update/update/update.component';
import { HomeComponent } from './components/home/home.component';
import { ListComponent } from './components/list/list/list.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { RouteGuardService } from './services/route-guard.service';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent, canActivate: [RouteGuardService] },
  { path: "employee/list", component: ListComponent, canActivate: [RouteGuardService] },
  { path: "employee/add", component: AddComponent, canActivate: [RouteGuardService] },
  { path: "employee/update/:id", component: UpdateComponent, canActivate: [RouteGuardService] },
  { path: "employee/details/:id", component: HomeComponent, canActivate: [RouteGuardService] },
  { path: "login", component: LoginComponent },
  { path: "logout", component: LogoutComponent, canActivate: [RouteGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}