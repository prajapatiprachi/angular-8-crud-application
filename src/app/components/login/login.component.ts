import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/models/login';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userName: string;
  public password: string;
  public userNameIsRequired: string = "Username is required";
  public passwordIsRequired: string = "password is required";
  public passwordPattern: string = "Password must contain one upercase, one lowercase, and min 8 digit";
  public login = new Login() ;
  public isInvalidUser: boolean;
 
  constructor(private _authenticationService : AuthenticationService, private router : Router) { }

  ngOnInit(): void {
  }

  public loginHandler(form) {

    if(this._authenticationService.authentication(this.login.userName, this.login.password)) {
      this.router.navigate(['/welcome']);
      this.isInvalidUser = false;
    }
    this.isInvalidUser = true;
  }

}
