import { Component, OnInit } from '@angular/core';
import {EmployeeServiceService} from "../../../services/employee-service.service"

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public employeeListResponse: any;
  public index: number= 0;

  constructor(private service: EmployeeServiceService) { }

  ngOnInit(): void {
    let employeeList = this.service.getEmployees();
    employeeList.subscribe((data)=>this.employeeListResponse=data);
  
  }

  public deleteEmployee(id) {
    this.service.deleteEmployee(id).subscribe(
      (data)=> this.employeeListResponse = data
    );
  }


}
