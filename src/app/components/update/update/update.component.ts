import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeServiceService } from 'src/app/services/employee-service.service';
import { List } from 'src/app/models/list';
import { IemployeeResponse } from 'src/app/interfaces/iemployee-response';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  public nameIsRequired: string = "Name is required";
  public positionIsRequired: string = "Position is required";
  public addressIsRequired: string = "Address is required";
  public name: string;
  public position: string ;
  public address: string;
public employeeId: any;
public employeeData : List;
public updateEmployee = new List();

  constructor(private router_link: ActivatedRoute, private _employeeService: EmployeeServiceService) {
    this.router_link.params.subscribe((param)=>this.employeeId = param.id)
   }

  ngOnInit(): void {
    this.getEmployeeById()
  }

  public getEmployeeById() {
    this._employeeService.getEmployeeById(this.employeeId).subscribe(
      response => 
        this.employeeData = response
    );
  }

  public submit(employeForm):void {
    this.updateEmployee.id=this.employeeId;
    this.updateEmployee.name= this.updateEmployee.name? this.updateEmployee.name : this.employeeData.name;
    this.updateEmployee.address= this.updateEmployee.address? this.updateEmployee.address : this.employeeData.address;
    this.updateEmployee.position= this.updateEmployee.position? this.updateEmployee.position : this.employeeData.position;
    this._employeeService.updateEmployees(this.updateEmployee).subscribe();
    employeForm.resetForm();
  }

}
