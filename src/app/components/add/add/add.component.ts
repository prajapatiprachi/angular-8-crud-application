import { Component, OnInit } from '@angular/core';
import { Add } from "../../../models/add";
import { EmployeeServiceService } from '../../../services/employee-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  public name: string;
  public position: string;
  public address: string;
  public nameIsRequired: string = "Employee name is required."
  public positionIsRequired: string = "Employee position is required."
  public addressIsRequired: string = "Employee address is required."
  public employee = new Add();

  constructor(private _employeeService: EmployeeServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  public submitHandler(employeeForm): void {
    this._employeeService.save(this.employee).subscribe();
    console.log(alert("Details save successfully."))
    employeeForm.resetForm();
    this.router.navigate(['/employee/list',])
  }
}
