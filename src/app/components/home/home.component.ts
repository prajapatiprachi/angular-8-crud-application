import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeServiceService } from 'src/app/services/employee-service.service';
import { Add } from '../../models/add';
import { IemployeeResponse } from 'src/app/interfaces/iemployee-response';
import { List } from 'src/app/models/list';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public employeeId:any;
  public employee: List;
  
  constructor(private routes: ActivatedRoute, private _employeeService: EmployeeServiceService) {
    this.routes.params.subscribe((param)=> this.employeeId = param.id)
   }

  ngOnInit(): void {
    this.getEmployeeById()
  }

  public getEmployeeById() {
    this._employeeService.getEmployeeById(this.employeeId).subscribe(
      response =>
        this.employee = response   
    );
  }
}
